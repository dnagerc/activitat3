import javax.swing.JOptionPane;
public class Cadenes_7 {

	public static void main(String[] args) {

		// Declaracions de variables
		String cadenaBinariInvertida="",cadenaBinari="";
		boolean esValid=false;
		int residu, nombre=0;
		
		try {

			// Demano un nombre decimal major o igual a 0			
			while(!esValid) {
				// Guardo l'entrada en base decimal
				nombre = Integer.parseInt(
						JOptionPane.showInputDialog(null,"Introdueix un nombre a convertir: ","Conversor a binari:",JOptionPane.QUESTION_MESSAGE));				
				// Comprovo si és vàlida
				esValid = (nombre >= 0);
			}

			// Si és major a 0, calculo el binari
			if (nombre > 0) {
				
				while (nombre != 1) {
					residu = nombre % 2;
					nombre = nombre / 2;
					cadenaBinariInvertida += residu+"";				
				}
				
				// Últim nombre
				if (nombre == 1) cadenaBinariInvertida+="1";	
				else if (nombre == 0) cadenaBinariInvertida+="0";					
				
				// Inverteixo els nombres
				for (int i = cadenaBinariInvertida.length()-1; i >= 0; i--) {
					cadenaBinari += cadenaBinariInvertida.charAt(i);
				}

			// Si és 0, només mostro un 0
			} else {
				cadenaBinari = "0";
			}
			
			// Ho mostro per consola
			System.out.println("El número en binari, és: "+cadenaBinari);

			// Ho mostro per pantalla 
			JOptionPane.showMessageDialog(null, "El número en binari, és: "+cadenaBinari);
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Hi ha hagut un error a l'entrada, surts del programa.");
		}
		
	}

}
